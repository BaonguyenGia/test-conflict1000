using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.Encryptions;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class SupportContactService : ISupportContactService
    {

        private string _connectionString;
        private ICryptoEncryptionHelper _cryptoEncryptionHelper;
        private IHttpContextHelper _httpContextHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<SupportContactService> _logger;

        public SupportContactService(IMapper mapper, ILogger<SupportContactService> logger, IConfiguration config,
            ICryptoEncryptionHelper cryptoEncryptionHelper, IHttpContextHelper httpContextHelper)
        {
            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _cryptoEncryptionHelper = cryptoEncryptionHelper;
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<SupportContactResource>> CreateSupportContact(SupportContactResource supportContactResource)
        {
            const string loggerHeader = "CreateSupportContact";

            var apiResponse = new ApiResponse<SupportContactResource>();
            SupportContact supportContact = _mapper.Map<SupportContactResource, SupportContact>(supportContactResource);

            _logger.LogDebug($"{loggerHeader} - Start to add SupportContact: {JsonConvert.SerializeObject(supportContact)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    supportContact.CreatedBy = _httpContextHelper.GetCurrentUser();
                    supportContact.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.SupportContactRepository.Add(supportContact);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new SupportContact successfully with Id: {supportContact.Id}");
                    supportContact = await unitOfWork.SupportContactRepository.FindFirst(predicate: d => d.Id == supportContact.Id,
                                                                           include: source => source.Include(d => d.PersonalProfile));
                    apiResponse.Data = _mapper.Map<SupportContact, SupportContactResource>(supportContact);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<SupportContactResource>> UpdateSupportContact(long id, SupportContactResource supportContactResource)
        {
            const string loggerHeader = "UpdateSupportContact";
            var apiResponse = new ApiResponse<SupportContactResource>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var supportContact = await unitOfWork.SupportContactRepository.FindFirst(predicate: d => d.Id == id,
                                                                                                include: source => source.Include(d => d.PersonalProfile));
                    supportContact = _mapper.Map<SupportContactResource, SupportContact>(supportContactResource, supportContact);
                    _logger.LogDebug($"{loggerHeader} - Start to update SupportContact: {JsonConvert.SerializeObject(supportContact)}");

                    supportContact.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    supportContact.LastModified = DateTime.UtcNow;
                    unitOfWork.SupportContactRepository.Update(supportContact);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update SupportContact successfully with Id: {supportContact.Id}");

                    supportContact = await unitOfWork.SupportContactRepository.FindFirst(d => d.Id == supportContact.Id);
                    apiResponse.Data = _mapper.Map<SupportContact, SupportContactResource>(supportContact);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<SupportContactResource>> DeleteSupportContact(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteSupportContact";

            var apiResponse = new ApiResponse<SupportContactResource>();

            _logger.LogDebug($"{loggerHeader} - Start to delete SupportContact with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var supportContact = await unitOfWork.SupportContactRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.SupportContactRepository.Remove(supportContact);
                    }
                    else
                    {
                        supportContact.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        supportContact.IsDeleted = true;
                        supportContact.LastModified = DateTime.UtcNow;
                        unitOfWork.SupportContactRepository.Update(supportContact);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete SupportContact successfully with Id: {supportContact.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<SupportContactResource>> GetSupportContact(long id)
        {
            const string loggerHeader = "GetSupportContact";

            var apiResponse = new ApiResponse<SupportContactResource>();

            _logger.LogDebug($"{loggerHeader} - Start to get SupportContact with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var supportContact = await unitOfWork.SupportContactRepository.FindFirst(predicate: d => d.Id == id,
                        include: source => source.Include(d => d.PersonalProfile));
                    apiResponse.Data = _mapper.Map<SupportContact, SupportContactResource>(supportContact);
                    _logger.LogDebug($"{loggerHeader} - Get SupportContact successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResultResource<SupportContactResource>>> GetSupportContacts(QueryResource queryObj)
        {
            const string loggerHeader = "GetSupportContacts";

            var apiResponse = new ApiResponse<QueryResultResource<SupportContactResource>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get SupportContacts with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<SupportContact, object>>>()
                    {
                        ["title"] = s => s.Title,
                        ["phoneNumber"] = s => s.PhoneNumber,
                    };

                    var query = await unitOfWork.SupportContactRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                                && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%")),
                                                                        include: source => source.Include(d => d.PersonalProfile),
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = _mapper.Map<QueryResult<SupportContact>, QueryResultResource<SupportContactResource>>(query);
                    _logger.LogDebug($"{loggerHeader} - Get SupportContacts successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}