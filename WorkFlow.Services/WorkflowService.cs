﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.API.Models;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class WorkflowService : IWorkflowService
    {

        private readonly IMapper _mapper;
        private readonly ILogger<WorkflowService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public WorkflowService(IMapper mapper, ILogger<WorkflowService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {

            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<WorkflowResource>> CreateWorkflow(WorkflowResource workflowResource)
        {
            const string loggerHeader = "CreateWorkflow";

            var apiResponse = new ApiResponse<WorkflowResource>();
            Workflow workflow = _mapper.Map<WorkflowResource, Workflow>(workflowResource);

            _logger.LogDebug($"{loggerHeader} - Start to addWorkflow: {JsonConvert.SerializeObject(workflow)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    workflow.CreatedBy = _httpContextHelper.GetCurrentUser();
                    workflow.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.WorkflowRepository.Add(workflow);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add newWorkflow successfully with Id: {workflow.Id}");
                    workflow = await unitOfWork.WorkflowRepository.FindFirst(d => d.Id == workflow.Id);
                    apiResponse.Data = _mapper.Map<Workflow, WorkflowResource>(workflow);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowResource>> UpdateWorkflow(long id, WorkflowResource workflowResource)
        {
            const string loggerHeader = "UpdateWorkflow";
            var apiResponse = new ApiResponse<WorkflowResource>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workflow = await unitOfWork.WorkflowRepository.FindFirst(predicate: d => d.Id == id);
                    workflow = _mapper.Map<WorkflowResource, Workflow>(workflowResource, workflow);
                    _logger.LogDebug($"{loggerHeader} - Start to updateWorkflow: {JsonConvert.SerializeObject(workflow)}");

                    workflow.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    workflow.LastModified = DateTime.UtcNow;
                    unitOfWork.WorkflowRepository.Update(workflow);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - UpdateWorkflow successfully with Id: {workflow.Id}");

                    workflow = await unitOfWork.WorkflowRepository.FindFirst(d => d.Id == workflow.Id);
                    apiResponse.Data = _mapper.Map<Workflow, WorkflowResource>(workflow);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowResource>> DeleteWorkflow(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteWorkflow";

            var apiResponse = new ApiResponse<WorkflowResource>();

            _logger.LogDebug($"{loggerHeader} - Start to deleteWorkflow with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workflow = await unitOfWork.WorkflowRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.WorkflowRepository.Remove(workflow);
                    }
                    else
                    {
                        workflow.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        workflow.IsDeleted = true;
                        workflow.LastModified = DateTime.UtcNow;
                        unitOfWork.WorkflowRepository.Update(workflow);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - DeleteWorkflow successfully with Id: {workflow.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowResource>> GetWorkflow(long id)
        {
            const string loggerHeader = "UpdateWorkflow";

            var apiResponse = new ApiResponse<WorkflowResource>();

            _logger.LogDebug($"{loggerHeader} - Start to getWorkflow with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workflow = await unitOfWork.WorkflowRepository.FindFirst(predicate: d => d.Id == id,
                                                                                    include: source => source.Include(d => d.Properties)
                                                                                                                .ThenInclude(p => p.Setting)
                                                                                                                    .ThenInclude(s => s.Choices)
                                                                                                              .Include(d => d.Category));

                    apiResponse.Data = _mapper.Map<Workflow, WorkflowResource>(workflow);
                    _logger.LogDebug($"{loggerHeader} - GetWorkflow successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResultResource<WorkflowResource>>> GetWorkflows(QueryResource queryObj)
        {
            const string loggerHeader = "GetWorkflows";

            var apiResponse = new ApiResponse<QueryResultResource<WorkflowResource>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to getWorkflows with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<Workflow, object>>>()
                    {
                        ["title"] = s => s.Title,
                        ["code"] = s => s.Code
                    };

                    var query = await unitOfWork.WorkflowRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%"))
                                                                                            && (!queryObj.Status.HasValue || d.Status == queryObj.Status.Value)
                                                                                            && (!queryObj.CategoryId.HasValue || d.CategoryId == queryObj.CategoryId.Value)
                                                                                            && (String.IsNullOrEmpty(queryObj.CreatePermission) || d.CreatePermission.Contains(queryObj.CreatePermission))
                                                                                            && (String.IsNullOrEmpty(queryObj.SeenPermission) || d.CreatePermission.Contains(queryObj.SeenPermission)),
                                                                        include: source => source.Include(d => d.Category).Include(d => d.Properties).ThenInclude(p => p.DefaultType)
                                                                                                  .Include(d => d.Properties).ThenInclude(p => p.Setting).ThenInclude(s => s.Choices),
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = _mapper.Map<QueryResult<Workflow>, QueryResultResource<WorkflowResource>>(query);
                    _logger.LogDebug($"{loggerHeader} - GetWorkflows successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowResource>> UploadWorkflowImage(long id, IFormFile file)
        {
            const string loggerHeader = "UploadWorkflowImage";

            var apiResponse = new ApiResponse<WorkflowResource>();

            if (file == null || file.Length == 0)
            {
                apiResponse.IsError = true;
                apiResponse.Message = "File not selected";
                return apiResponse;
            }
            else if (file.Length > 5242880)
            {
                apiResponse.IsError = true;
                apiResponse.Message = "File must be smaller than 5MB";
                return apiResponse;
            }
            else if (!file.IsImage())
            {
                apiResponse.IsError = true;
                apiResponse.Message = "File must be an image";
                return apiResponse;
            }
            else
            {
                _logger.LogDebug($"{loggerHeader} - Start to Upload WorkflowImage with");
                using (var unitOfWork = new UnitOfWork(_connectionString))
                {
                    try
                    {
                        string dir = Path.Combine("wwwroot", "WorkflowImages");
                        if (!Directory.Exists(dir))
                        {
                            Directory.CreateDirectory(dir);
                        }
                        string fileName = id + "_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".png";
                        var newPath = Path.Combine(dir, fileName);

                        _logger.LogDebug($"{loggerHeader} - Save file in new path: {newPath}");
                        using (var stream = new FileStream(newPath, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }

                        var workFlow = await unitOfWork.WorkflowRepository.FindFirst(d => d.Id == id);

                        var oldPath = workFlow.ImageURL;
                        _logger.LogDebug($"{loggerHeader} - Delete file in old path: {oldPath}");
                        if (File.Exists(oldPath))
                        {
                            File.Delete(oldPath);
                        }

                        _logger.LogDebug($"{loggerHeader} - Upload ImageURL for Id: {workFlow.Id}");
                        workFlow.ImageURL = newPath;
                        workFlow.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        workFlow.LastModified = DateTime.UtcNow;
                        unitOfWork.WorkflowRepository.Update(workFlow);
                        await unitOfWork.SaveChanges();
                        _logger.LogDebug($"{loggerHeader} - Upload WorkflowImage successfully with Id: {workFlow.Id}");

                        apiResponse.Data = _mapper.Map<Workflow, WorkflowResource>(workFlow);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                        apiResponse.IsError = true;
                        apiResponse.Message = ex.Message;
                        await unitOfWork.SaveErrorLog(ex);
                    }
                    finally
                    {
                        unitOfWork.Dispose();
                    }
                }
            }
            return apiResponse;
        }

        public async Task<ApiResponse<FileStream>> GetWorkflowImage(long id)
        {
            const string loggerHeader = "GetWorkflowImage";
            var apiResponse = new ApiResponse<FileStream>();
            _logger.LogDebug($"{loggerHeader} - Start to get WorkflowImage with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workFlow = await unitOfWork.WorkflowRepository.FindFirst(d => d.Id == id);
                    var image = File.OpenRead(workFlow.ImageURL);
                    apiResponse.Data = image;
                    _logger.LogDebug($"{loggerHeader} - Get WorkflowImage successfully with Id: {workFlow.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }
            return apiResponse;
        }

        public async Task<ApiResponse<WorkflowResource>> UploadWorkflowImageBase64(long id, string base64String)
        {
            const string loggerHeader = "UploadWorkflowImageBase64";
            var apiResponse = new ApiResponse<WorkflowResource>();

            _logger.LogDebug($"{loggerHeader} - Start to upload WorkflowImageBase64 with");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    string dir = Path.Combine("wwwroot", "WorkflowImages");
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    string fileName = id + "_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".png";
                    var newPath = Path.Combine(dir, fileName);

                    //Convert base 64 string to byte[]
                    byte[] imageBytes = Convert.FromBase64String(base64String);

                    _logger.LogDebug($"{loggerHeader} - Save file in new path: {newPath}");
                    using (var stream = new FileStream(newPath, FileMode.Create))
                    {
                        stream.Write(imageBytes, 0, imageBytes.Length);
                    }

                    var workFlow = await unitOfWork.WorkflowRepository.FindFirst(d => d.Id == id);

                    var oldPath = workFlow.ImageURL;
                    _logger.LogDebug($"{loggerHeader} - Delete file in old path: {oldPath}");
                    if (File.Exists(oldPath))
                    {
                        File.Delete(oldPath);
                    }
                    _logger.LogDebug($"{loggerHeader} - Upload ImageURL for Id: {workFlow.Id}");
                    workFlow.ImageURL = newPath;
                    workFlow.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    workFlow.LastModified = DateTime.UtcNow;
                    unitOfWork.WorkflowRepository.Update(workFlow);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Upload WorkflowImageBase64 successfully with Id: {workFlow.Id}");
                    apiResponse.Data = _mapper.Map<Workflow, WorkflowResource>(workFlow);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }
            return apiResponse;
        }
    }
}
