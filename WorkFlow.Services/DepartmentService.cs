﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly IMapper _mapper;
        private readonly ILogger<DepartmentService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public DepartmentService(IMapper mapper, ILogger<DepartmentService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {
            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<DepartmentResource>> CreateDepartment(DepartmentResource departmentResource)
        {
            const string loggerHeader = "CreateDepartment";

            var apiResponse = new ApiResponse<DepartmentResource>();
            Department department = _mapper.Map<DepartmentResource, Department>(departmentResource);

            _logger.LogDebug($"{loggerHeader} - Start to add department: {JsonConvert.SerializeObject(department)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    department.CreatedBy = _httpContextHelper.GetCurrentUser();
                    department.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.DepartmentRepository.Add(department);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new department successfully with Id: {department.Id}");
                    department = await unitOfWork.DepartmentRepository.FindFirst(d => d.Id == department.Id);
                    apiResponse.Data = _mapper.Map<Department, DepartmentResource>(department);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<DepartmentResource>> UpdateDepartment(long id, DepartmentResource departmentResource)
        {
            const string loggerHeader = "UpdateDepartment";
            var apiResponse = new ApiResponse<DepartmentResource>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var department = await unitOfWork.DepartmentRepository.FindFirst(d => d.Id == id);
                    department = _mapper.Map<DepartmentResource, Department>(departmentResource, department);
                    _logger.LogDebug($"{loggerHeader} - Start to update department: {JsonConvert.SerializeObject(department)}");

                    department.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    department.LastModified = DateTime.UtcNow;
                    unitOfWork.DepartmentRepository.Update(department);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update department successfully with Id: {department.Id}");

                    department = await unitOfWork.DepartmentRepository.FindFirst(d => d.Id == department.Id);
                    apiResponse.Data = _mapper.Map<Department, DepartmentResource>(department);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<DepartmentResource>> DeleteDepartment(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteDepartment";

            var apiResponse = new ApiResponse<DepartmentResource>();

            _logger.LogDebug($"{loggerHeader} - Start to delete department with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var department = await unitOfWork.DepartmentRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.DepartmentRepository.Remove(department);
                    }
                    else
                    {
                        department.IsDeleted = true;
                        department.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        department.LastModified = DateTime.UtcNow;
                        unitOfWork.DepartmentRepository.Update(department);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete department successfully with Id: {department.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<DepartmentResource>> GetDepartment(long id)
        {
            const string loggerHeader = "UpdateDepartment";

            var apiResponse = new ApiResponse<DepartmentResource>();

            _logger.LogDebug($"{loggerHeader} - Start to get department with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var department = await unitOfWork.DepartmentRepository.FindFirst(d => d.Id == id);
                    apiResponse.Data = _mapper.Map<Department, DepartmentResource>(department);
                    _logger.LogDebug($"{loggerHeader} - Get department successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResultResource<DepartmentResource>>> GetDepartments(QueryResource queryObj)
        {
            const string loggerHeader = "GetDepartments";

            var apiResponse = new ApiResponse<QueryResultResource<DepartmentResource>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get departments with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<Department, object>>>()
                    {
                        ["title"] = s => s.Title,
                        ["deptlevel"] = s => s.DeptLevel
                    };

                    var query = await unitOfWork.DepartmentRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%"))
                                                                                            && (!queryObj.ManagerId.HasValue || d.ManagerId == queryObj.ManagerId.Value)
                                                                                            && (!queryObj.DepartmentId.HasValue || d.ParentId == queryObj.DepartmentId.Value),
                                                                        include: source => source.Include(d => d.Manager).Include(d => d.Parent),
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);

                    apiResponse.Data = _mapper.Map<QueryResult<Department>, QueryResultResource<DepartmentResource>>(query);
                    _logger.LogDebug($"{loggerHeader} - Get departments successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<List<DepartmentTreeView>>> GetTreeViewDepartments(QueryResource queryObj)
        {
            const string loggerHeader = "GetTreeViewDepartments";

            var apiResponse = new ApiResponse<List<DepartmentTreeView>>();
            _logger.LogDebug($"{loggerHeader} - Start to get departments tree view with");
            var departmentTreeViews = new List<DepartmentTreeView>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<Department, object>>>()
                    {
                        ["title"] = s => s.Title,
                        ["deptlevel"] = s => s.DeptLevel
                    };

                    var query = await unitOfWork.DepartmentRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && d.ParentId == null,
                                                                        include: null,
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderBy(d => d.Order).ThenByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true);

                    foreach (var department in query.Items)
                    {
                        await RecursionGetDepartmentTreeView(unitOfWork, departmentTreeViews, department.Id, departmentTreeViews.Count);
                    }

                    apiResponse.Data = departmentTreeViews;
                    _logger.LogDebug($"{loggerHeader} - Get departments successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        private async Task RecursionGetDepartmentTreeView(UnitOfWork unitOfWork, List<DepartmentTreeView> departmentTreeViews,
            long id, int parentIndex)
        {
            var department = await unitOfWork.DepartmentRepository.FindFirst(predicate: d => d.Id == id && d.IsDeleted == false,
                                                                    include: source => source.Include(d => d.ChildDepartments),
                                                                    disableTracking: true);

            if (department != null)
            {
                var children = department.ChildDepartments.Where(c => c.IsDeleted == false).ToList();
                if (children.Count == 0)
                {
                    if (parentIndex == departmentTreeViews.Count)
                    {
                        departmentTreeViews.Add(new DepartmentTreeView
                        {
                            Label = department.Title,
                            Id = department.Id,
                            ParentId = department.ParentId,
                            Items = null
                        });
                    }
                    else
                    {
                        departmentTreeViews[departmentTreeViews.Count - 1].Items.Add(
                            new DepartmentTreeView
                            {
                                Label = department.Title,
                                Id = department.Id,
                                ParentId = department.ParentId,
                                Items = null
                            }
                        );
                    }
                }
                else
                {
                    departmentTreeViews.Add(new DepartmentTreeView
                    {
                        Label = department.Title,
                        Id = department.Id,
                        ParentId = department.ParentId,
                        Items = new List<DepartmentTreeView>()
                    });
                    var newParentIndex = departmentTreeViews.Count - 1;
                    foreach (var child in children)
                    {
                        await RecursionGetDepartmentTreeView(unitOfWork, departmentTreeViews, child.Id, newParentIndex);
                    }
                }
            }
        }
    }
}
