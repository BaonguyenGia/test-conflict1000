﻿using System;
using System.Threading.Tasks;

namespace WorkFlow.Infrastructure.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        Task SaveChanges();
    }
}
