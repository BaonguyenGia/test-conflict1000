using System.ComponentModel.DataAnnotations;

namespace WorkFlow.Services.Models
{
    public class SupportContact : EngineEntity
    {
        public string Title { get; set; }
        public string PhoneNumber { get; set; }
        [Required]
        public long PersonalProfileId { get; set; }
        public PersonalProfile PersonalProfile { get; set; }
    }
}
