﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkFlow.Services.Models
{
    public class Workflow : EngineEntity
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public WorkflowStatus Status { get; set; }
        public string Version { get; set; }
        public string CreatePermission { get; set; }
        public string SeenPermission { get; set; }
        public string ImageURL { get; set; }
        public bool IsAttachmentActive { get; set; }
        public string AllowAttachmentExtensions { get; set; }
        public int MaximumSizeSingle { get; set; }
        public int MaximumSizeTotal { get; set; }
        public long? CategoryId { get; set; }
        public int? Order { get; set; }
        #region  Foreign
        [ForeignKey("CategoryId")]
        public WorkflowCategory Category { get; set; }
        [ForeignKey("WorkflowId")]
        public ICollection<Property> Properties { get; set; }
        #endregion
    }
}
