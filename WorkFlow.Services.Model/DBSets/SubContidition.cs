﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkFlow.Services.Models
{
    public class SubContidition : EngineEntity
    {
        [ForeignKey("WorkflowCondition")]
        [Required]
        public long? WorkflowConditionId { get; set; }
        [Required]
        public string Variable { get; set; }
        [Required]
        public string Conditional { get; set; }
        [Required]
        public string Value { get; set; }
        [Required]
        public string NexCondition { get; set; }
        [Required]
        public int Order { get; set; }
    }
}
