﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkFlow.Services.Models
{
    public class WorkflowCondition : EngineEntity
    {
        [Required]
        [ForeignKey("WorkflowStep")]
        public long WorkflowStepId { get; set; }
        [Required]
        public string Title { get; set; }
        public int NextStep { get; set; }
        public int ReturnStep { get; set; }
        public bool RunNextCondition { get; set; }
        public ICollection<SubContidition> SubContiditions { get; set; }
    }
}
