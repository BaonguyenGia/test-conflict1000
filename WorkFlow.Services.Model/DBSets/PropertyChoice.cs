﻿namespace WorkFlow.Services.Models
{
    public class PropertyChoice : EngineEntity
    {
        public string Title { get; set; }
        public long SettingId { get; set; }
        #region Foreign
        public PropertySetting Setting { get; set; }
        #endregion
    }
}
