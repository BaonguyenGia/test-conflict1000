﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkFlow.Services.Models
{
    public class WorkingTime : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        [ForeignKey("Workweek")]
        public long? WorkweekId { get; set; }
        public double MorningStart { get; set; }
        public double MorningEnd { get; set; }
        public double AfternoonStart { get; set; }
        public double AfternoonEnd { get; set; }
        #region Foreign
        public Workweek Workweek { get; set; }
        #endregion
    }
}
