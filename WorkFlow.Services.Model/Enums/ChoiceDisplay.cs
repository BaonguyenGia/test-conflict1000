﻿using System.ComponentModel;

namespace WorkFlow.Services.Models
{
    public enum ChoiceDisplay
    {
        [Description("Drop-Down Menu")]
        DropDownMenu,
        [Description("Radio Buttons")]
        RadioButton,
        [Description("Checkboxes (allow multiple selections)")]
        CheckBoxes,
        [Description("MultiSelect")]
        MultiSelect
    }
}
