﻿namespace WorkFlow.API.DataContracts
{
    public class Base64File
    {
        public string Base64 { get; set; }
        public string Extension { get; set; }
    }
}