﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class WorkflowConditionResource : EngineEntity
    {
        [Required]
        public long WorkflowStepId { get; set; }
        [Required]
        public string Title { get; set; }
        public int NextStep { get; set; }
        public int ReturnStep { get; set; }
        public bool RunNextCondition { get; set; }
        public ICollection<SubContiditionResource> SubContiditions { get; set; }
    }
}
