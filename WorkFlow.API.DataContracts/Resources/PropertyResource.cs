﻿using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class PropertyResource : EngineEntity
    {
        public string Name { get; set; }
        public string Label { get; set; }
        public bool IsRequired { get; set; }
        public bool IsViewOnly { get; set; }
        public bool IsSingle { get; set; }
        public bool IsMultiLine { get; set; }
        public bool IsChoice { get; set; }
        public bool IsNumber { get; set; }
        public bool IsCurrency { get; set; }
        public bool IsDateAndTime { get; set; }
        public bool IsLookup { get; set; }
        public bool IsYesNo { get; set; }
        public bool IsUser { get; set; }
        public bool IsCalculated { get; set; }
        public string StyleClass { get; set; }
        public string ExecuteJS { get; set; }
        public string Description { get; set; }
        public int? Order { get; set; }
        public string DefaultValue { get; set; }
        public long? DefaultTypeId { get; set; }
        public long? SettingId { get; set; }
        public long? WorkflowId { get; set; }
        public PropertyDefaultValueTypeResource DefaultType { get; set; }
        public PropertySettingResource Setting { get; set; }
    }
}
