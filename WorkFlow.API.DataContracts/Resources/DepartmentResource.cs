﻿using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class DepartmentResource : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        public string Label { get; set; }
        [Required]
        public string Code { get; set; }
        public string Description { get; set; }
        public long? ParentId { get; set; }
        public string ChartCode { get; set; }
        public long? ManagerId { get; set; }
        public int DeptLevel { get; set; }
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string Email { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public string SiteName { get; set; }
        public int? Order { get; set; }
    }
}
