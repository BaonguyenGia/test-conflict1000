﻿using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class WorkflowCategoryResource : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        public int Order { get; set; }
    }
}
