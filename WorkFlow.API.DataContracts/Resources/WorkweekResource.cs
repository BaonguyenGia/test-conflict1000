﻿using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class WorkweekResource : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        public bool IsFullTime { get; set; }
    }
}
