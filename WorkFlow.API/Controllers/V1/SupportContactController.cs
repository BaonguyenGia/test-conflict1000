using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/supportContacts")]//required for default versioning
    [Route("api/v{version:apiVersion}/supportContacts")]
    [ApiController]
    public class SupportContactController : Controller
    {
        private ISupportContactService _supportContactService;
        public SupportContactController(ISupportContactService supportContactService)
        {
            _supportContactService = supportContactService;
        }

        // GET: api/supportContacts
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _supportContactService.GetSupportContacts(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/supportContacts/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _supportContactService.GetSupportContact(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/supportContacts
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] SupportContactResource supportContactResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _supportContactService.CreateSupportContact(supportContactResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/supportContacts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] SupportContactResource supportContactResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _supportContactService.UpdateSupportContact(id, supportContactResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/supportContacts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _supportContactService.DeleteSupportContact(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
