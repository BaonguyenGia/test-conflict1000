﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/Workflows")]//required for default versioning
    [Route("api/v{version:apiVersion}/Workflows")]
    [ApiController]
    public class WorkflowController : Controller
    {
        private IWorkflowService _workflowService;

        public WorkflowController(IWorkflowService workflowService)
        {
            _workflowService = workflowService;
        }

        // GET: api/Workflows
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _workflowService.GetWorkflows(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/Workflows/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _workflowService.GetWorkflow(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/Workflows
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] WorkflowResource workflowResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workflowService.CreateWorkflow(workflowResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/Workflows/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] WorkflowResource workflowResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workflowService.UpdateWorkflow(id, workflowResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/Workflows/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _workflowService.DeleteWorkflow(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        [HttpPost]
        [Route("{id}/uploadWorkflowImage")]
        public async Task<IActionResult> UploadWorkflowImage(long id, IFormFile file)
        {
            var apiResponse = await _workflowService.UploadWorkflowImage(id, file);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        [HttpGet]
        [Route("{id}/getWorkflowImage")]
        public async Task<IActionResult> GetWorkflowImage(long id)
        {
            var apiResponse = await _workflowService.GetWorkflowImage(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : File(apiResponse.Data, "image/png");
        }

        [HttpPost]
        [Route("{id}/uploadWorkflowImageBase64")]
        public async Task<IActionResult> UploadWorkflowImageBase64(long id, [FromBody] Base64File base64File)
        {
            var apiResponse = await _workflowService.UploadWorkflowImageBase64(id, base64File.Base64);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
