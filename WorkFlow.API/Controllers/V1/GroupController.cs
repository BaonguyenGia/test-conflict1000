﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/groups")]//required for default versioning
    [Route("api/v{version:apiVersion}/groups")]
    [ApiController]

    public class groupController : Controller
    {
        private IGroupService _groupService;

        public groupController(IGroupService groupService)
        {
            _groupService = groupService;
        }

        // GET: api/groups
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _groupService.GetGroups(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/groups/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _groupService.GetGroup(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/groups
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] GroupResource groupResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _groupService.CreateGroup(groupResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/groups/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] GroupResource groupResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _groupService.UpdateGroup(id, groupResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/groups/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _groupService.DeleteGroup(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}

