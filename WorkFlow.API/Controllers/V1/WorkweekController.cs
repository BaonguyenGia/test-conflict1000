﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/workweeks")]//required for default versioning
    [Route("api/v{version:apiVersion}/workweeks")]
    [ApiController]
    public class WorkweekController : Controller
    {
        private IWorkweekService _workweekService;

        public WorkweekController(IWorkweekService workweekService)
        {
            _workweekService = workweekService;
        }

        // GET: api/workweeks
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _workweekService.GetWorkweeks(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/workweeks/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _workweekService.GetWorkweek(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/workweeks
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] WorkweekResource WorkweekResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workweekService.CreateWorkweek(WorkweekResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/workweeks/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] WorkweekResource WorkweekResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _workweekService.UpdateWorkweek(id, WorkweekResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/workweeks/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _workweekService.DeleteWorkweek(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
