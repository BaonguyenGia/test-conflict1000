﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/personalProfiles")]//required for default versioning
    [Route("api/v{version:apiVersion}/personalProfiles")]
    [ApiController]
    public class PersonalProfileController : Controller
    {
        private IPersonalProfileService _personalProfileService;

        public PersonalProfileController(IPersonalProfileService personalProfileService)
        {
            this._personalProfileService = personalProfileService;
        }

        // GET: api/personalProfiles
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _personalProfileService.GetPersonalProfiles(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/personalProfiles/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _personalProfileService.GetPersonalProfile(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/personalProfiles
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PersonalProfileResource PersonalProfileResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _personalProfileService.CreatePersonalProfile(PersonalProfileResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/personalProfiles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] PersonalProfileResource PersonalProfileResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _personalProfileService.UpdatePersonalProfile(id, PersonalProfileResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/personalProfiles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _personalProfileService.DeletePersonalProfile(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        [HttpPost]
        [Route("{id}/uploadAvatar")]
        public async Task<IActionResult> UploadPersonalProfileAvatar(long id, IFormFile file)
        {
            var apiResponse = await _personalProfileService.UploadPersonalProfileAvatar(id, file);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        [HttpGet]
        [Route("{id}/getAvatarById")]
        public async Task<IActionResult> GetPersonalProfileAvatar(long id)
        {
            var apiResponse = await _personalProfileService.GetPersonalProfileAvatar(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : File(apiResponse.Data, "image/png");
        }

        [HttpGet]
        [Route("{email}/getAvatarByEmail")]
        public async Task<IActionResult> GetPersonalProfileAvatar(string email)
        {
            var apiResponse = await _personalProfileService.GetPersonalProfileAvatar(email);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : File(apiResponse.Data, "image/png");
        }

        [HttpPost]
        [Route("{id}/uploadAvatarBase64")]
        public async Task<IActionResult> UploadPersonalProfileAvatarBase64(long id, [FromBody] Base64File base64File)
        {
            var apiResponse = await _personalProfileService.UploadPersonalProfileAvatarBase64(id, base64File.Base64);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
