﻿import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { IDocumentTypeModel } from "@Models/IDocumentTypeModel";
import { IQueryResult } from "@Models/IQueryResult";

export default class DocumentTypeService extends ServiceBase {
    public async search(
        title: string = null
    ): Promise<Result<IQueryResult<IDocumentTypeModel>>> {
        if (title == null) {
            title = "";
        }
        var result = await this.requestJson<IQueryResult<IDocumentTypeModel>>({
            url: `api/documentTypes`,
            method: "GET",
            data: { title: title },
        });
        return result;
    }

    public async update(model: IDocumentTypeModel): Promise<Result<IDocumentTypeModel>> {
        var result = await this.requestJson({
            url: `/api/documentTypes/${model.id}`,
            method: "PUT",
            data: model,
        });
        return result;
    }

    public async delete(id: number): Promise<Result<{}>> {
        var result = await this.requestJson({
            url: `/api/documentTypes/${id}`,
            method: "DELETE",
        });
        return result;
    }

    public async add(model: IDocumentTypeModel): Promise<Result<IDocumentTypeModel>> {
        var result = await this.requestJson<number>({
            url: "/api/documentTypes",
            method: "POST",
            data: model,
        });
        return result;
    }
}
