import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { IWorkingTimesModel } from "@Models/IWorkingTimesModel";

import { IQueryResult } from "@Models/IQueryResult";

export default class WorkingTimesService extends ServiceBase {
  public async search(
    title: string = null
  ): Promise<Result<IQueryResult<IWorkingTimesModel>>> {
    if (title == null) {
      title = "";
    }
    var result = await this.requestJson<IQueryResult<IWorkingTimesModel>>({
      url: `/api/workingTimes`,
      method: "GET",
      data: { title: title },
    });
    return result;
  }


  public async update(model: IWorkingTimesModel): Promise<Result<IWorkingTimesModel>> {
    var result = await this.requestJson({
      url: `/api/workingTimes/${model.id}`,
      method: "PUT",
      data: model,
    });
    return result;
  }

  
  public async delete(id: number): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/workingTimes/${id}`,
      method: "DELETE",
    });
    return result;
  }

  public async add(model: IWorkingTimesModel): Promise<Result<IWorkingTimesModel>> {
    var result = await this.requestJson<number>({
      url: "/api/workingTimes",
      method: "POST",
      data: model,
    });
    return result;
  }
}
