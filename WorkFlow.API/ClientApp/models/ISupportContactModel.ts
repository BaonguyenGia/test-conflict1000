﻿import { IEngineEntity } from "@Models/IEngineEntity";

export interface ISupportContactModel extends IEngineEntity {
    title: string;
    phoneNumber: string;
    personalProfileId: number;
}