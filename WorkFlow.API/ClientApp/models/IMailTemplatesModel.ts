import { IEngineEntity } from "@Models/IEngineEntity";


export interface IMailTemplatesModel extends IEngineEntity {
    name: string;
    mailModuleId: number;
    subject: string;
    subjectEN: string;
    subjectParameters: string;
    content: string;
    contentParameters: string;
}