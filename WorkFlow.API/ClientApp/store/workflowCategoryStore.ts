import { createSlice, PayloadAction, Dispatch } from "@reduxjs/toolkit";
import { IWorkflowCategoryModel } from "@Models/IWorkflowCategoryModel";
import WorkflowCategoryService from "@Services/WorkflowCategoryService";

// Declare an interface of the store's state.
export interface IWorkflowCategoryStoreState {
  isFetching: boolean;
  items: IWorkflowCategoryModel[];
}

// Create the slice.
const slice = createSlice({
  name: "workflowCategory",
  initialState: {
    isFetching: false,
    items: [],
  } as IWorkflowCategoryStoreState,
  reducers: {
    setFetching: (state, action: PayloadAction<boolean>) => {
      state.isFetching = action.payload;
    },
    setData: (state, action: PayloadAction<IWorkflowCategoryModel[]>) => {
      state.items = action.payload;
    },
    addData: (state, action: PayloadAction<IWorkflowCategoryModel>) => {
      state.items = [...state.items, action.payload];
    },
    updateData: (state, action: PayloadAction<IWorkflowCategoryModel>) => {
      // We need to clone collection (Redux-way).
      var items = [...state.items];
      var entryIndex = items.findIndex((x) => x.id === action.payload.id);
      if (entryIndex >= 0) {
        items[entryIndex] = action.payload;
        state.items = items;
      }
    },
    deleteData: (state, action: PayloadAction<{ id: number }>) => {
      state.items = state.items.filter((x) => x.id !== action.payload.id);
    },
  },
});

// Export reducer from the slice.
export const { reducer } = slice;

// Define action creators.
export const actionCreators = {
  search: (title?: string) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new WorkflowCategoryService();

    const result = await service.search(title);

    if (!result.hasErrors) {
      dispatch(slice.actions.setData(result.value.items));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  add: (model: IWorkflowCategoryModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new WorkflowCategoryService();

    const result = await service.add(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.addData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  update: (model: IWorkflowCategoryModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new WorkflowCategoryService();

    const result = await service.update(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.updateData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  remove: (id: number) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new WorkflowCategoryService();

    const result = await service.delete(id);

    if (!result.hasErrors) {
      dispatch(slice.actions.deleteData({ id }));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
};
