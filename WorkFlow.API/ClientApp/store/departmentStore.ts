import { createSlice, PayloadAction, Dispatch } from "@reduxjs/toolkit";
import { IDepartmentModel } from "@Models/IDepartmentModel";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import { IDepartmentTreeViewModel } from "@Models/IDepartmentTreeViewModel";
import DepartmentService from "@Services/DepartmentService";
import PersonalProfileService from "@Services/PersonalProfileService";

// Declare an interface of the store's state.
export interface IDepartmentStoreState {
  isFetching: boolean;
  items: IDepartmentModel[];
  departmentTreeViews: IDepartmentTreeViewModel[];
}

// Create the slice.
const slice = createSlice({
  name: "department",
  initialState: {
    isFetching: false,
    items: [],
  } as IDepartmentStoreState,
  reducers: {
    setFetching: (state, action: PayloadAction<boolean>) => {
      state.isFetching = action.payload;
    },
    setData: (state, action: PayloadAction<IDepartmentModel[]>) => {
      state.items = action.payload;
    },
    setDepartmentTreeViewData: (
      state,
      action: PayloadAction<IDepartmentTreeViewModel[]>
    ) => {
      state.departmentTreeViews = action.payload;
    },
    addData: (state, action: PayloadAction<IDepartmentModel>) => {
      state.items = [...state.items, action.payload];
    },
    updateData: (state, action: PayloadAction<IDepartmentModel>) => {
      // We need to clone collection (Redux-way).
      var items = [...state.items];
      var entryIndex = items.findIndex((x) => x.id === action.payload.id);
      if (entryIndex >= 0) {
        items[entryIndex] = action.payload;
        state.items = items;
      }
    },
    deleteData: (state, action: PayloadAction<{ id: number }>) => {
      state.items = state.items.filter((x) => x.id !== action.payload.id);
    },
  },
});

// Export reducer from the slice.
export const { reducer } = slice;

// Define action creators.
export const actionCreators = {
  search: (title?: string) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new DepartmentService();

    const result = await service.search(title);

    if (!result.hasErrors) {
      dispatch(slice.actions.setData(result.value.items));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  searchDepartmentTreeView: () => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new DepartmentService();

    const result = await service.searchDepartmentTreeView();

    if (!result.hasErrors) {
      dispatch(slice.actions.setDepartmentTreeViewData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  add: (model: IDepartmentModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new DepartmentService();

    const result = await service.add(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.addData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  update: (model: IDepartmentModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new DepartmentService();

    const result = await service.update(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.updateData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  remove: (id: number) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new DepartmentService();

    const result = await service.delete(id);

    if (!result.hasErrors) {
      dispatch(slice.actions.deleteData({ id }));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
};
