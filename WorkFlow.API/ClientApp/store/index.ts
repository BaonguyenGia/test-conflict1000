import * as loginStore from "@Store/loginStore";
import * as personStore from "@Store/personStore";
import * as positionStore from "@Store/positionStore";
import * as departmentStore from "@Store/departmentStore";
import * as workflowCategoryStore from "@Store/workflowCategoryStore";
import * as workWeekStore from "@Store/workWeekStore";
import * as personalProfileStore from "@Store/personalProfileStore";
import * as workingTimesStore from "@Store/workingTimesStore";
import * as documentTypeStore from "@Store/documentTypeStore";
import * as workflowStore from "@Store/workflowStore";
import * as mailTemplatesStore from "@Store/mailTemplatesStore";
import * as supportContactStore from "@Store/supportContactStore";
import { connect } from "react-redux";

// The top-level state object.
export interface IApplicationState {
  login: loginStore.ILoginStoreState;
  person: personStore.IPersonStoreState;
  position: positionStore.IPositionStoreState;
  department: departmentStore.IDepartmentStoreState;
  workflowCategory: workflowCategoryStore.IWorkflowCategoryStoreState;
  workweek: workWeekStore.IWorkWeekStoreState
  personalProfile: personalProfileStore.IPersonalProfileStoreState;
  documentType: documentTypeStore.IDocumentTypeStoreState;
  workflow: workflowStore.IWorkflowStoreState;
  workingTimes: workingTimesStore.IWorkingTimesStoreState;
  mailTemplates: mailTemplatesStore.IMailTemplatesState;
  supportContact: supportContactStore.ISupportContactStoreState;

}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
  login: loginStore.reducer,
  person: personStore.reducer,
  position: positionStore.reducer,
  department: departmentStore.reducer,
  workflowCategory: workflowCategoryStore.reducer,
  workweek: workWeekStore.reducer,
  personalProfile: personalProfileStore.reducer,
  documentType: documentTypeStore.reducer,
  workflow: workflowStore.reducer,
  workingTimes: workingTimesStore.reducer,
  mailTemplates: mailTemplatesStore.reducer,
  supportContact: supportContactStore.reducer,
};

// This type can be used as a hint on action creators so that its 'dispatch' and 'getState' params are
// correctly typed to match your store.
export interface IAppThunkAction<TAction> {
  (
    dispatch: (action: TAction) => void,
    getState: () => IApplicationState
  ): void;
}

export interface IAppThunkActionAsync<TAction, TResult> {
  (
    dispatch: (action: TAction) => void,
    getState: () => IApplicationState
  ): Promise<TResult>;
}


export function withStore<
  TStoreState,
  TActionCreators,
  TComponent extends React.ComponentType<TStoreState & TActionCreators & any>
>(
  component: TComponent,
  stateSelector: (state: IApplicationState) => TStoreState,
  actionCreators: TActionCreators
): TComponent {
  return <TComponent>(
    (<unknown>connect(stateSelector, actionCreators)(component))
  );
}
