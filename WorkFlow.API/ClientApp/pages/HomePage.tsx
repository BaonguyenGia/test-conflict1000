import * as React from "react";
import { RouteComponentProps } from "react-router";
import { Helmet } from "react-helmet";
import { X, Plus } from "react-bootstrap-icons";
type Props = RouteComponentProps<{}>;
type Propsitem = {
  name?: string;
  onpressdelete?: () => void;
  onpressadd?: () => void;
  link?: string;
};
const Item: React.FC<Propsitem> = ({
  name,
  onpressadd,
  onpressdelete,
  link,
}: Propsitem) => {
  return (
    <li>
      <span className="groupName">
        <a href={link}>{name}</a>
      </span>
      <span
        onClick={onpressadd}
        style={{ verticalAlign: "top", cursor: "pointer" }}
      >
        <Plus size={28} color="green" />
      </span>
      <span
        onClick={onpressadd}
        style={{ verticalAlign: "top", cursor: "pointer" }}
      >
        <X size={28} color="red" />
      </span>
      <span style={{ clear: "both" }}></span>
    </li>
  );
};
const HomePage: React.FC<Props> = () => {
  const Arraynameitem: string[] = [
    "Admin",
    "All",
    "Approve by mail",
    "BDH - Ban Dieu Hanh",
    "Approvers",
    "BDH - Ban Dieu Hanh",
    "BDH - Ban Dieu Hanh - Manager",
    "Becamex",
    "Becamex - Becamex",
    "Becamex - Becamex - Manager",
    "Binh Phuoc",
    "Designers",
    "Excel Services Viewers",
    "Group IT",
    "GroupManager",
    "HiddenMenu",
    "HOIDONGQUANTRI",
    "HOIDONGQUANTRI - HOI DONG QUAN TRI",
    "HOIDONGQUANTRI - HOI DONG QUAN TRI - Manager",
    "Home Members",
    "Home Owners",
    "Home Visitors",
    "MEP - Phong Co - Dien (M.E.P)",
    "HOIDONGQUANTRI - HOI DONG QUAN TRI - Manager",
    "MPTEST - Phong Ban Test",
    "MPTEST - Phong Ban Test - Manager",
    "PVHDC - Phong Van Hanh DC",
    "PVHDC - Phong Van Hanh DC - Manager",
    "QL Nhan vien",
    "QL phe duyet",
    "QLTrucTiepXe",
    "QLTrucTiepXeTest",
    "Quick Deploy Users",
    "R&D - Trung tam nghien cuu & phat trien (R&D)",
    "R&D - Trung tam nghien cuu & phat trien (R&D) - Manager",
    "Restricted Readers",
    "Send SMS",
    "Style Resource Readers",
    "test1401",
    "TTL - To tro ly",
    "TTL - To tro ly - Manager",
    "TTL - Trung tam Nghien cuu & Phat trien kiem Thanh vien TTL - kiem bien phien dich tieng anh",
    "TTL - Trung tam Nghien cuu & Phat trien kiem Thanh vien TTL - kiem bien phien dich tieng anh - Manager",
    "View All",
    "VNTT - Cong Ty Co Phan Cong Nghe & Truyen Thong Viet Nam",
    "VNTT - Cong Ty Co Phan Cong Nghe & Truyen Thong Viet Nam - Manager",
    "Xem bao cao",
    "Xem đơn xin nghỉ phép",
    "Xoa Quy Trinh",
  ];

  return (
    <div className="form01">
      <Helmet>
        <title>Home page - WorkFlow.API</title>
      </Helmet>
      <div className="mt-5">
        <p style={{ fontSize: 18, color: "#005296", fontWeight: 700 }}>
          Thông tin quản trị{" "}
        </p>
      </div>
      <div className="button01">
        <Plus size={30} color="green" />
        <a>Tạo nhóm</a>
      </div>
      <div className="formInformation">
        <div className="itemRow">
          <div className="itemText">Dung lượng hệ thống tối đa</div>
          <div className="itemInput">Không giới hạn</div>
        </div>
        <div className="itemRow01">
          <div className="itemText">Dung lượng đã sử dụng</div>
          <div className="itemInput">7 MB</div>
        </div>
        <div className="itemRow01">
          <div className="itemText">Số lượng tài khoản tối đa</div>
          <div className="itemInput">0</div>
        </div>
        <div className="itemRow01">
          <div className="itemText">Số lượng tài khoản hiện tại</div>
          <div className="itemInput">
            <a href="#">237</a>
          </div>
        </div>
        <div className="itemRow01">
          <div className="itemText1">Các nhóm trên site</div>
          <div className="itemInput">
            <ul className="listGroup">
              {Arraynameitem.map((item) => (
                <Item name={item} link="#" />
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
