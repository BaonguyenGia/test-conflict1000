import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { IWorkflowModel } from "@Models/IWorkflowModel";
import * as WorkflowStore from "@Store/workflowStore";
import { withStore } from "@Store/index";
import Paginator from "@Components/shared/Paginator";
import Information from "@Components/workflow/Information";
import { Container } from "react-bootstrap";
import ProgressStep from "@Components/workflow/ProgressStep";
import { Save2Fill, XCircleFill } from "react-bootstrap-icons";
import Result from "@Core/Result";

type Props = typeof WorkflowStore.actionCreators &
  WorkflowStore.IWorkflowStoreState &
  RouteComponentProps<{}>;

interface IState {
  steps: string[];
  activeIndex: number;
  workflow: IWorkflowModel;
  errorMessage: string;
}

class WorkflowAddPage extends React.Component<Props, IState> {
  private paginator: Paginator;

  constructor(props: Props) {
    super(props);

    this.state = {
      steps: [
        "Tạo quy trình",
        "Thuộc tính Form",
        "Thiết kế form",
        "Thiết kế form",
        "Phát hành",
      ],
      activeIndex: 0,
      workflow: {} as IWorkflowModel,
      errorMessage: "",
    };

    this.props.searchPersonalProfiles();
    this.props.searchWorkflowCategory();
  }

  saveWorkflow = async () => {
    const { activeIndex, workflow } = this.state;
    const { add } = this.props;

    if (activeIndex === 0) {
      if (!workflow.title || workflow.title.trim() === "") {
        this.setState({ errorMessage: "Vui lòng điền têu đề" });
      } else {
        var result = (await add(workflow)) as any as Result<IWorkflowModel>;

        if (!result.hasErrors) {
          this.setState({ activeIndex: activeIndex + 1 });
        }
      }
    }
  };

  render() {
    const { isFetching, personalProfiles, workflowCategories } = this.props;
    const { steps, activeIndex, workflow, errorMessage } = this.state;

    return (
      <Container>
        <Helmet>
          <title>WorkflowDefine - Khoản mục mới</title>
        </Helmet>
        <div className="workflow-main-progress">
          <ProgressStep
            steps={steps}
            activeIndex={activeIndex}
            onChangeStep={(index) => this.setState({ activeIndex: index })}
          />
        </div>

        <div className="workflow-main-container">
          <div className="workflow-main-option">
            {errorMessage.length > 0 && (
              <div className="error-message">{errorMessage}</div>
            )}
            <div className="option-panel">
              <div className="add" onClick={this.saveWorkflow}>
                <Save2Fill size={20} color="orange" /> Tiếp tục
              </div>

              <div>
                <XCircleFill size={20} color="red" /> Thoát
              </div>
            </div>
          </div>
          <div className="direct">
            <Information
              workflowParent={workflow}
              personalProfiles={personalProfiles}
              workflowCategories={workflowCategories}
              updateMainData={(data: IWorkflowModel) => {
                this.setState({
                  workflow: data,
                });
              }}
            />
          </div>
        </div>
      </Container>
    );
  }
}

// Connect component with Redux store.
var connectedComponent = withStore(
  WorkflowAddPage,
  (state) => state.workflow, // Selects which state properties are merged into the component's props.
  WorkflowStore.actionCreators // Selects which action creators are merged into the component's props.
);

// Attach the React Router to the component to have an opportunity
// to interract with it: use some navigation components,
// have an access to React Router fields in the component's props, etc.
export default withRouter(connectedComponent);
