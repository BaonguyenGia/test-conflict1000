import * as React from "react";
import { IWorkingTimesModel  } from "@Models/IWorkingTimesModel";
import { Formik, Field } from "formik";
import { Form, Row  } from "react-bootstrap";
import FormValidator from "@Components/shared/FormValidator";
import { MAX_LENGTH_INPUT, MAX_LENGTH_TEXTAREA } from "@Constants";


export interface IProps {
  data: IWorkingTimesModel;
  isEditted: boolean | null;
  onSubmit: (data: IWorkingTimesModel) => void;
  children: (
    renderEditor: () => JSX.Element,
    submit: () => void
  ) => JSX.Element;
}

const WorkingTimesEditor: React.FC<IProps> = (props: IProps) => {
  const formValidator = React.useRef<FormValidator>(null);
  
  const onSubmitForm = (values: IWorkingTimesModel) => {
    
    if(values.morningStart)
    if (!formValidator.current.isValid()) {
      // Form is not valid.
      return;
    }
    props.onSubmit(values);
  };

  // This function will be passed to children components as a parameter.
  // It's necessary to build custom markup with controls outside this component.
  const renderEditor = (values: IWorkingTimesModel, isEditted: boolean | null) => {
    return (
      // {({ errors, touched }) => ()
      <FormValidator className="form" ref={(x) => (formValidator.current = x)}>
        <div className="form-group">
          <Field name={nameof.full<IWorkingTimesModel>((x) => x.title)}>
            {({ field }) => (
              <>
                <label
                  className="control-label required"
                  htmlFor="position__firstName"
                >
                  Tiêu đề:
                </label>
                <input
                  
                  type="text"
                  className="form-control"
                  id="position__title"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="true"
                  data-msg-required="Vui lòng điền tiêu đề."
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IWorkingTimesModel>((x) => x.morningStart)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="position__description"
                >
                  Thời gian bắt đầu buổi sáng:
                </label>
                  <input
                  type="number"
                  className="form-control"
                  placeholder="Vui lòng nhập số..."
                  id="position__title"
                  name={field.name}
                  data-value-type="number"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IWorkingTimesModel>((x) => x.morningEnd)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="position__description"
                >
                  Thời gian kết thúc buổi sáng:
                </label>
                  <input
                  type="number"
                  className="form-control"
                  placeholder="Vui lòng nhập số..."
                  id="position__title"
                  name={field.name}
                  data-value-type="number"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IWorkingTimesModel>((x) => x.afternoonStart)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="position__description"
                >
                  Thời gian bắt đầu buổi chiều:
                </label>
                  <input
                  type="number"
                  className="form-control"
                  placeholder="Vui lòng nhập số..."
                  id="position__title"
                  name={field.name}
                  data-value-type="number"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IWorkingTimesModel>((x) => x.afternoonEnd)}>
            {({ field}) => (
              <>
                <label
                  className="control-label"
                  htmlFor="position__description"
                >
                  Thời gian kết thúc buổi chiều:
                </label>
                  <input
                  type="number"
                  className="form-control"
                  placeholder="Vui lòng nhập số..."
                  id="position__title"
                  name={field.name}
                  data-value-type="number"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>

        {isEditted && (
          <div>
            <div className="form-group-divide">
              

              
            </div>
          </div>
        )}
      </FormValidator>
    );
  };

  return (
    <Formik
      enableReinitialize
      initialValues={props.data}
      onSubmit={(values, { setSubmitting }) => {
        onSubmitForm(values);
      }}
     
    >
      {({ values, handleSubmit }) => {
        // Let's say that the children element is a parametrizable function.
        // So we will pass other elements to this functional component as children
        // elements of this one:
        // <PersonEditor>
        // {(renderEditor, handleSubmit) => <>
        //     {renderEditor()}
        //     <button onClick={x => handleSubmit()}>Submit</button>
        // </>}
        // </PersonEditor>.
        return props.children(
          () => renderEditor(values, props.isEditted),
          handleSubmit
        );
      }}
    </Formik>
  );
};


export default WorkingTimesEditor;
