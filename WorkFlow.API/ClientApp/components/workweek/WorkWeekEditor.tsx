import * as React from "react";
import { IWorkWeekModel  } from "@Models/IWorkWeekModel";
import { Formik, Field } from "formik";
import FormValidator from "@Components/shared/FormValidator";
import { formatTimeInEntity } from "@Services/FormatDateTimeService";
import { MAX_LENGTH_INPUT, MAX_LENGTH_TEXTAREA } from "@Constants";

export interface IProps {
  data: IWorkWeekModel;
  isEditted: boolean | null;
  onSubmit: (data: IWorkWeekModel) => void;
  children: (
    renderEditor: () => JSX.Element,
    submit: () => void
  ) => JSX.Element;
}

const WorkWeekEditor: React.FC<IProps> = (props: IProps) => {
  const formValidator = React.useRef<FormValidator>(null);

  const onSubmitForm = (values: IWorkWeekModel) => {
    if (!formValidator.current.isValid()) {
      // Form is not valid.
      return;
    }
    props.onSubmit(values);
  };

  // This function will be passed to children components as a parameter.
  // It's necessary to build custom markup with controls outside this component.
  const renderEditor = (values: IWorkWeekModel, isEditted: boolean | null) => {
    return (
      <FormValidator className="form" ref={(x) => (formValidator.current = x)}>
        <div className="form-group">
          <Field name={nameof.full<IWorkWeekModel>((x) => x.title)}>
            {({ field }) => (
              <>
                <label
                  className="control-label required"
                  htmlFor="position__firstName"
                >
                  Tiêu đề:
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="position__title"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="true"
                  data-msg-required="Vui lòng điền tiêu đề."
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IWorkWeekModel>((x) => x.isFullTime)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="position__description"
                >
                  Làm việc fulltime:
                </label>
                <Field  type="checkbox" name={field.name} onChange={field.onChange} />
              </>
            )}
          </Field>
        </div>

        {isEditted && (
          <div>
            <div className="form-group-divide">
            </div>
          </div>
        )}
      </FormValidator>
    );
  };

  return (
    <Formik
      enableReinitialize
      initialValues={props.data}
      onSubmit={(values, { setSubmitting }) => {
        onSubmitForm(values);
      }}
    >
      {({ values, handleSubmit }) => {
        // Let's say that the children element is a parametrizable function.
        // So we will pass other elements to this functional component as children
        // elements of this one:
        // <PersonEditor>
        // {(renderEditor, handleSubmit) => <>
        //     {renderEditor()}
        //     <button onClick={x => handleSubmit()}>Submit</button>
        // </>}
        // </PersonEditor>.
        return props.children(
          () => renderEditor(values, props.isEditted),
          handleSubmit
        );
      }}
    </Formik>
  );
};


export default WorkWeekEditor;
