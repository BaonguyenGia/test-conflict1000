import React, { useState } from "react";

export interface IProps {
  steps: string[];
  activeIndex: number;
  onChangeStep: (index: number) => void;
}

const ProgressStep: React.FC<IProps> = (props: IProps) => {
  const { steps, activeIndex } = props;

  return (
    <div>
      <div className="workflowStepGuide">
        {steps &&
          steps.map((step, index) => (
            <div
              key={index}
              onClick={() => props.onChangeStep(index)}
              className="workflowStepGuideDiv"
            >
              <div
                className={`step-process ${
                  activeIndex === index ? " selected" : ""
                }`}
              >
                {index !== 0 && <span className="first"></span>}
                <span className="text"> {step} </span>
                <span
                  className={
                    index === steps.length - 1 ? "last-final last" : "last"
                  }
                ></span>
              </div>
            </div>
          ))}
      </div>
    </div>
  );
};

export default ProgressStep;
