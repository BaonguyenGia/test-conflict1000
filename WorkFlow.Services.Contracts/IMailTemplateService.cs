﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IMailTemplateService
    {
        Task<ApiResponse<MailTemplateResource>> CreateMailTemplate(MailTemplateResource mailTemplateResource);
        Task<ApiResponse<MailTemplateResource>> UpdateMailTemplate(long id, MailTemplateResource mailTemplateResource);
        Task<ApiResponse<MailTemplateResource>> DeleteMailTemplate(long id, bool removeFromDB = false);
        Task<ApiResponse<MailTemplateResource>> GetMailTemplate(long id);
        Task<ApiResponse<QueryResultResource<MailTemplateResource>>> GetMailTemplates(QueryResource queryObj);
    }
}
