﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IPositionService
    {
        Task<ApiResponse<PositionResource>> CreatePosition(PositionResource PositionResource);
        Task<ApiResponse<PositionResource>> UpdatePosition(long id, PositionResource PositionResource);
        Task<ApiResponse<PositionResource>> DeletePosition(long id, bool removeFromDB = false);
        Task<ApiResponse<PositionResource>> GetPosition(long id);
        Task<ApiResponse<QueryResultResource<PositionResource>>> GetPositions(QueryResource queryObj);
    }
}
