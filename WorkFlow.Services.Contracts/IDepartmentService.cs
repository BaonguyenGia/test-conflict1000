﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IDepartmentService
    {
        Task<ApiResponse<DepartmentResource>> CreateDepartment(DepartmentResource departmentResource);
        Task<ApiResponse<DepartmentResource>> UpdateDepartment(long id, DepartmentResource departmentResource);
        Task<ApiResponse<DepartmentResource>> DeleteDepartment(long id, bool removeFromDB = false);
        Task<ApiResponse<DepartmentResource>> GetDepartment(long id);
        Task<ApiResponse<QueryResultResource<DepartmentResource>>> GetDepartments(QueryResource queryObj);
        Task<ApiResponse<List<DepartmentTreeView>>> GetTreeViewDepartments(QueryResource queryResource);
    }
}
