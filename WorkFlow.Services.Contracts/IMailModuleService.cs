﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IMailModuleService
    {
        Task<ApiResponse<MailModuleResource>> CreateMailModule(MailModuleResource mailModuleResource);
        Task<ApiResponse<MailModuleResource>> UpdateMailModule(long id, MailModuleResource mailModuleResource);
        Task<ApiResponse<MailModuleResource>> DeleteMailModule(long id, bool removeFromDB = false);
        Task<ApiResponse<MailModuleResource>> GetMailModule(long id);
        Task<ApiResponse<QueryResultResource<MailModuleResource>>> GetMailModules(QueryResource queryObj);
    }
}
