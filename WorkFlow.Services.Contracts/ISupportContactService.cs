using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface ISupportContactService
    {
        Task<ApiResponse<SupportContactResource>> CreateSupportContact(SupportContactResource SupportContactResource);
        Task<ApiResponse<SupportContactResource>> UpdateSupportContact(long id, SupportContactResource SupportContactResource);
        Task<ApiResponse<SupportContactResource>> DeleteSupportContact(long id, bool removeFromDB = false);
        Task<ApiResponse<SupportContactResource>> GetSupportContact(long id);
        Task<ApiResponse<QueryResultResource<SupportContactResource>>> GetSupportContacts(QueryResource queryObj);
    }
}
