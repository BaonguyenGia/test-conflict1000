﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkflowStepService
    {
        Task<ApiResponse<WorkflowStepResource>> CreateWorkflowStep(WorkflowStepResource workflowStepResource);
        Task<ApiResponse<List<WorkflowStepResource>>> CreateWorkflowSteps(List<WorkflowStepResource> workflowStepResources);
        Task<ApiResponse<WorkflowStepResource>> UpdateWorkflowStep(long id, WorkflowStepResource workflowStepResource);
        Task<ApiResponse<WorkflowStepResource>> DeleteWorkflowStep(long id, bool removeFromDB = false);
        Task<ApiResponse<WorkflowStepResource>> GetWorkflowStep(long id);
        Task<ApiResponse<QueryResultResource<WorkflowStepResource>>> GetWorkflowSteps(QueryResource queryObj);
    }
}
